#!/bin/sh

# depends: sox, metaflac (flac), basename, dirname, mkdir, rm, getconf, awk, sed
# -converts 24 bit FLAC or WAV files @ 44.1 / 48 / 88.2 / 96 / 176.4 / 192 KHz to 16 bit FLAC files @ 44.1 or 48 KHz
# -preserves source-embedded artwork, sets tags for sox command, source specs, and source ffp, and adds padding

# <- options ->
jobs="0" # the number of threads to use, set "0" to attempt detection and use of all available CPU cores
outdir_name="converted" # the name of the output directory (saved in source file's parent dir)
padding="4096"    # use "0" to disable, or the amount of padding in bytes (ie: characters) to add to the output flac file.
add_tags="1"      # set "1" to enable adding SOURCE_FFP, SOURCE_SPECS, and SOX_COMMAND tags to output files
embed_artwork="1" # set "1" to preserve embedded artwork from source (if it exists) in output
tier_one_sources="1" # set "1" to convert 24/44 and 24/48 sources to 16/44 and 16/48
tier_two_outputs="1" # set "1" to convert 24/176 and 24/192 sources to 24/88 and 24/96
wav_in_wav_out="0"   # set "1" to output in wav format for wav inputs (otherwise wav inputs produce flac outputs)
sox_emits="1" # set "1" to show upon encode completion, any status/messages/warnings emitted by sox

# sox command construction: $> sox $sox_pre_input $input_flac $sox_pre_output $sox_bits $output_flac $sox_rate $rate $sox_dither
# $sox_bits and $sox_dither are unset for 24 bit outputs, $sox_rate and $rate are unset for 24/44 and 24/48 sources
sox_pre_input="-G -R -V2" # sox 'Global Options', followed by any 'File Format Options' for the input file
sox_pre_output=""         # sox 'File Format Options' for to the output file, excluding '-b', which is set, or not, automatically
sox_rate="rate -v -L"     # sox 'rate' command (not including target sample rate, which is set, or not, automatically)
sox_dither="dither"       # sox 'dither' command
# <- end of options ->

[ "$#" -ge "1" ] || { echo 'Please provide one or more flac files as arguments, or "--help" to display help text.' >&2 ;exit 1 ; }

if [ -x /usr/local/bin/brew ] ;then
	homebrew_bin="/usr/local/bin"
elif [ -x /opt/homebrew/bin/brew ] ;then
	homebrew_bin="/opt/homebrew/bin"
fi
[ -n "$homebrew_bin" ] && PATH=$PATH:"$homebrew_bin"

set -- "$@" _break_loop_here_
while true ;do
	case $1 in
		-h|-H|--help)
			cat <<EOF
Usage:
  $> dss-posix.sh flac_file|-h|-H|--help [options]... [flac_file]... [wav_file]...

Options:
  -1, --tier-one
  --1, --no-tier-one
            enable / disable use of tier one sources
            when enabled: 24/48 and 24/44.1 are converted to 16/48 and 16/44.1
            when disabled: 24/48 and 24/44.1 sources are skipped

  -2, --tier-two
  --2, --no-tier-two
            enable / disable use of tier two outputs
            when enabled: 24/192 and 24/176.4 are converted to 24/96 and 24/88.2
            when disabled: 24/192 and 24/176.4 are converted to 16/48 and 16/44.1

  -a, --artwork
  -A, --no-artwork
            enable / disable re-embedding any source-embedded artwork

  -e, --emit-sox
  -E, --no-emit
            enable / disable display of sox's stdout & stderr output, if any

  -h, -H, --help
            show this help text and exit

  --hold    on completion, wait for user input before exiting

  -j N, --jobs N
            set the number of concurrent jobs to N
            set "0" to (attempt to) use all available threads

  -o NAME, --outdir-name NAME
            set the name of the output directory to NAME
            the directory is created in the parent of the source file
            quoting the argument may be necessary

  -p N, --padding N
            set the amount of flac padding in bytes (ie: characters) to N
            set "0" to disable padding

  -t, --tags
  -T, --no-tags
            enable / disable adding tags to detail source file and sox command
            tags added: SOURCE_FFP, SOURCE_SPECS, and SOX_COMMAND

  -w, --wiwo
  -W, --no-wiwo
            enable / disable using WAV outputs for WAV inputs

EOF
			exit ;;
		*.[Ff][Ll][Aa][Cc] | *.[Ww][Aa][Vv] )
			[ "$( sox --i -b -- "$1" )" != "24" ] && { shift ;continue ; }
			# thought: count # of wavs, # of flacs ... needs another case statement here? worth it for stdout info/flair??
			set -- "$@" "$1" ;shift ;;
		-1|--tier-one)
			tier_one_sources="1" ;shift ;;
		--1|--no-tier-one)
			tier_one_sources="0" ;shift ;;
		-2|--tier-two)
			tier_two_outputs="1" ;shift ;;
		--2|--no-tier-two)
			tier_two_outputs="0" ;shift ;;
		-a|--artwork)
			embed_artwork="1" ;shift ;;
		-A|--no-artwork)
			embed_artwork="0" ;shift ;;
		-e|--emit-sox)
			sox_emits="1" ;shift ;;
		-E|--no-emit)
			sox_emits="0" ;shift ;;
		--hold)
			hold="1" ;shift ;;
		-j|--jobs)
			jobs="$2" ;shift 2 ;;
		-o|--outdir-name)
			outdir_name="$2" ;shift 2 ;;
		-p|--padding)
			padding="$2" ;shift 2 ;;
		-t|--tags)
			add_tags="1" ;shift ;;
		-T|--no-tags)
			add_tags="0" ;shift ;;
		-w|--wiwo)
			wav_in_wav_out="1" ;shift ;;
		-W|--no-wiwo)
			wav_in_wav_out="0" ;shift ;;
	    _break_loop_here_)
			shift ;break ;;
		*)
			shift ;;
	esac
done

[ "$#" -ge "1" ] || { echo 'No [24 bit] FLAC or WAV files found in arguments.' >&2 ;exit 1 ; }
case $jobs in '' | *[!0-9]* ) echo '"jobs" option not set with a number, using single-thread.' >&2 ;jobs="1" ;; esac # https://stackoverflow.com/a/61835747
case $padding in '' | *[!0-9]* ) echo '"padding" option not set with a number, using default.' >&2 ;padding="4096" ;; esac

[ "$jobs" = "0" ] && { # https://unix.stackexchange.com/a/564512 | https://gist.github.com/jj1bdx/5746298
	jobs="$( getconf _NPROCESSORS_ONLN 2> /dev/null )" || # Linux and similar...
		jobs="$( getconf NPROCESSORS_ONLN 2> /dev/null )" || # FreeBSD (and derivatives), OpenBSD, MacOS and similar...
		jobs="$( ksh93 -c 'getconf NPROCESSORS_ONLN' 2> /dev/null )" || # Solaris and similar...
		{ echo "Unable to detect available cores, using single-thread." ;jobs="1" ; }
}

printf 'Converting %s source file(s) with %s concurrent SoX process(es) ...\n\n' "$#" "$( if [ "$jobs" -gt "$#" ] ;then printf '%s' "$#" ;else printf '%s' "$jobs" ;fi )"

_format_output() { # yo dog, I heard you like printf ...
	nl='
'
	# awk indents (each line of) $soxout, but in some modes, sox uses a carriage return on a
	# repeatedly-emitted (and newline-omitted) single line of its per-file stdout (an ongoing status/progress display)
	# sed replaces each carriage return with a carriage return followed by the same number of spaces awk indents all the other lines to
	printf -- ' -> %s\n    -> 24/%s (%s) -=> %s/%s (%s)%s\n    -> %s\n\n\n' \
		   \
		   "$src" \
		   "$(( rate / 1000 ))" "$srcextn" "$target_bits" "$target_khz" "$outextn" \
		   \
		   "$( [ -n "$soxout" ] && [ "$sox_emits" = "1" ] && printf -- '\n    -> Sox Output:\n       ->\n%s\n       ->' \
               \
                "$( printf -- '%s' "${soxout#"$nl"}" |awk -- '{ print "          " $0 }' |sed -- 's/\r/\r          /g' )"
           )" \
			   "$1"
}

i="0"
while [ "$#" -gt "0" ] ;do

	until [ "$i" -eq "$jobs" ] || [ "$#" -eq "0" ] ;do
		src="$1"
		rate="$( sox --i -r -- "$src" )"
		case $rate in
			44100 | 48000)
				[ "$tier_one_sources" = "1" ] || { shift ;continue ; }
				target_bits="16" ;target_khz="$(( rate / 1000 ))" ;sox_bits="-b 16" ;rate_cmd="" ;dither_cmd="$sox_dither"
				;;
			88200) target_bits="16" ;target_khz="44" ;sox_bits="-b 16" ;rate_cmd="${sox_rate} 44100" ;dither_cmd="$sox_dither" ;;
			96000) target_bits="16" ;target_khz="48" ;sox_bits="-b 16" ;rate_cmd="${sox_rate} 48000" ;dither_cmd="$sox_dither" ;;
			192000)
				case $tier_two_outputs in
					1) target_bits="24" ;target_khz="96" ;sox_bits=""      ;rate_cmd="${sox_rate} 96000" ;dither_cmd="" ;;
					*) target_bits="16" ;target_khz="48" ;sox_bits="-b 16" ;rate_cmd="${sox_rate} 48000" ;dither_cmd="$sox_dither" ;;
				esac
				;;
			176400)
				case $tier_two_outputs in
					1) target_bits="24" ;target_khz="88" ;sox_bits=""      ;rate_cmd="${sox_rate} 88200" ;dither_cmd="" ;;
					*) target_bits="16" ;target_khz="44" ;sox_bits="-b 16" ;rate_cmd="${sox_rate} 44100" ;dither_cmd="$sox_dither" ;;
				esac
				;;
			*)
				shift ;continue ;;
		esac

		srcfile="$( basename -- "$src" )"
		srcbase="${srcfile%.*}"
		case ${srcfile##*.} in
			[Ff][Ll][Aa][Cc]) srcextn="flac" ;;
			[Ww][Aa][Vv])     srcextn="wav" ;;
			# aiff ?
		esac
		srcdir="$( dirname -- "$src" )"
		outdir="${srcdir}/${outdir_name}"
		if [ "$srcextn" = "wav" ] && [ "$wav_in_wav_out" = "1" ] ;then
			outextn="wav"
		else
			outextn="flac"
		fi
		outfile="${outdir}/${srcbase}.${outextn}"
		[ "$srcextn" = "flac" ] && artwork="${outfile}.metaflac.img" # && [ "$outextn" = "flac" ] # if flac-->wav supported later?

		                                                 # where will this error message slot in among any potential text outputs from other jobs?
		[ ! -d "$outdir" ] && { mkdir -p -- "$outdir" || { printf 'could not create directory: %s\n  ... skipping %s\n\n' "$outdir" "$src" >&2 ;shift ;continue ; } ; }

		{ if soxout="$( sox $sox_pre_input "$src" $sox_pre_output $sox_bits "$outfile" $rate_cmd $dither_cmd 2>&1 )" ;then
			  [ "$outextn" = "flac" ] && {

				  [ "$srcextn" = "flac" ] && [ "$embed_artwork" = "1" ] && {
					  metaflac --export-picture-to="${artwork}" -- "$src" >/dev/null 2>&1 &&
						  metaflac --import-picture-from="$artwork" -- "$outfile" &&
						  rm -- "$artwork"
				  }

				  [ "$padding" != "0" ] && metaflac --add-padding="$padding" -- "$outfile"

				  [ "$add_tags" = "1" ] && {
					  [ "$srcextn" = "flac" ] && metaflac --set-tag=SOURCE_FFP="$( metaflac --show-md5sum -- "$src" )" -- "$outfile"

					  metaflac --set-tag=SOURCE_SPECS="24 bit, $rate Hz" \
							   --set-tag=SOX_COMMAND="sox $sox_pre_input input.${srcextn} $sox_pre_output $sox_bits output.flac $rate_cmd $dither_cmd" \
							   -- "$outfile"
				  }
			  }
			  _format_output 'Succeeded!'
		  else
			  _format_output 'FAILED!'
		  fi
		} &
		shift
		i="$(( i + 1 ))"
	done

	# bash 'wait -n' can return exit codes of backgrounded jobs, posix wait can also? if not dss-posix,
	# dss-bash at least should detect (and maybe count?) non-zero exit codes, and provide its own exit status
	wait
	i="0"
done

[ "$hold" = "1" ] && {
	printf 'Done. Press enter to quit. '
	read -r REPLY
}
