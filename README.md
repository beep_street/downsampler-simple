# downsampler-simple

- Resample and/or downconvert 24-bit FLAC or WAV files with `sox`
- Output sample rates are automatically selected based on common multiples:
  | Input KHz    | Output BPS/KHz    |
  |:---:         |:---:              |
  | 192          | 24/96<sup>1</sup>, 16/48     |
  | 176.4        | 24/88.2<sup>1</sup>, 16/44.1 |
  | 96, 48<sup>2</sup>     | 16/48             |
  | 88.2, 44.1<sup>2</sup> | 16/44.1           |

  <sup>1</sup> when using `--tier-two` option, `-b 16` and `dither [...]` are omitted from `sox` invocation

  <sup>2</sup> when using `--tier-one` option, `rate ...` command is omitted from `sox` invocation
- Two script variants are provided:
  - `dss-posix.sh` provides per-*batch-of*-files-based multiprocessing under any POSIX-compliant shell
  - `dss-bash.sh` provides per-file-based multiprocessing under Bash version 4.3 or higher
  - at present there are no other differences

## Optional Functionality
- Output 24/88.2 and 24/96 from 176.4 and 192 KHz sources
- Downconvert 24/44.1 and 24/48 sources to 16 bit without resampling (or skip these sources)
- Perform follow-up tasks on FLAC outputs with `metaflac`
   - Add padding
   - Add tags detailing the source file and sox command (SOURCE_FFP, SOURCE_SPECS, and SOX_COMMAND)
   - Re-embed any artwork detected in source FLAC file (which sox otherwise discards)
- Output WAV from WAV sources (FLAC is the default output for both input formats)
- Automatically detect available threads (POSIX-ly with `getconf` -- not thoroughly tested)
- Display stdout/stderr from Sox
- Define your own Sox command
  - set 'Global Options'
  - set 'Input & Output File Format Options'
    - not tested, for 'raw' inputs, it's probably simpler to convert your 'raw' format files to FLAC or WAV first
    - `-b` is automatically set for output files
    - specification of varying input/output file characteristics in the same invocation is not supported
  - set the `rate` command to use
  - set the `dither` command to use
  - target sample rates are automatically set based on common multiples of source rates
  - target bit depths are automatically set based on source depths and script options

## Dependencies / Requirements:
- sox, metaflac (flac)
- posix utilities
  - basename, dirname, mkdir, rm, getconf, awk, sed
- `dss-posix.sh` requires a POSIX-compliant shell/interpeter
- `dss-bash.sh` requires Bash version 4.3 or higher

## Installation
- Copy/paste, or download each/either script, or clone the repository
- Place the script(s) in your $PATH and make it/them executable with `chmod +x ...`)

## Usage
`dss-posix.sh flac_file|wav_file|-h|-H|--help [options]... [flac_file]... [wav_file]...`

or

`dss-bash.sh flac_file|wav_file|-h|-H|--help [options]... [flac_file]... [wav_file]...`

## Runtime Options:
```
  -1, --tier-one
  --1, --no-tier-one
            enable / disable use of tier one sources
            when enabled: 24/48 and 24/44.1 are converted to 16/48 and 16/44.1
            when disabled: 24/48 and 24/44.1 sources are skipped

  -2, --tier-two
  --2, --no-tier-two
            enable / disable use of tier two outputs
            when enabled: 24/192 and 24/176.4 are converted to 24/96 and 24/88.2
            when disabled: 24/192 and 24/176.4 are converted to 16/48 and 16/44.1

  -a, --artwork
  -A, --no-artwork
            enable / disable re-embedding any source-embedded artwork

  -e, --emit-sox
  -E, --no-emit
            enable / disable display of sox's stdout & stderr output, if any

  -h, -H, --help
            show this help text and exit

  --hold    on completion, wait for user input before exiting

  -j N, --jobs N
            set the number of concurrent jobs to N
            set "0" to (attempt to) use all available threads

  -o NAME, --outdir-name NAME
            set the name of the output directory to NAME
            the directory is created in the parent of the source file
            quoting the argument may be necessary

  -p N, --padding N
            set the amount of flac padding in bytes (ie: characters) to N
            set "0" to disable padding

  -t, --tags
  -T, --no-tags
            enable / disable adding tags to detail source file and sox command
            tags added: SOURCE_FFP, SOURCE_SPECS, and SOX_COMMAND

  -w, --wiwo
  -W, --no-wiwo
            enable / disable using WAV outputs for WAV inputs
```
